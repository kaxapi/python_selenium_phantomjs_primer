from selenium import webdriver
import selenium.webdriver.support.ui as ui

driver = webdriver.PhantomJS('/home/kax/phantomjs/bin/phantomjs',
                              service_args=['--ignore-ssl-errors=true', '--ssl-protocol=any'])

# obtain wait object
wait = ui.WebDriverWait(driver,10)

# navigate 
driver.get('https://www.linkedin.com/uas/login')

# wait for element
wait.until(lambda driver: driver.find_element_by_id('session_key-login'))

# take screenshot
driver.save_screenshot('screenshot1.png')

# get elements
el_id = driver.find_element_by_id('session_key-login')
el_password = driver.find_element_by_id('session_password-login')

# enter text
el_id.send_keys('login')
el_password.send_keys('password')

# submit form/login
el_password.submit()

# wait for title
wait.until(lambda driver: driver.title.lower().startswith('welcome!'))

driver.save_screenshot('screenshot2.png')
driver.quit()

